class Character:
    def __init__(self, name, hp, hpmax, strength):
        self.name = name
        self.hp = hp
        self.hpmax = hpmax
        self.strength = strength

    def attack(self, target):
        target.hp -= self.strength
        if target.hp <= 0:
            target.hp = 0

        if target.name == "Link":
            print(f"You have been attacked by {self.name} and inflicted {self.strength} damage")
        elif target.name == "Bokoblin":
            print(f"You attacked {target.name} and inflicted {self.strength} damage")

    def heal(self):
        if self.hp < self.hpmax:
            self.hp += self.hpmax / 2
            print(f"You have retrieve {self.hp} hp")
            if self.hp >= self.hpmax:
                self.hp = self.hpmax
        else:
            print("you have already all your hp")
