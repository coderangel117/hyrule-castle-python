from character import Character


def display_ascci_art(filename: str):
    file = open(filename)
    content = file.read()
    print(content)
    file.close()


exit_floor = 11
boss_floor = 10

Hero = Character("Link", 60, 60, 15)
Enemy = Character("Bokoblin", 30, 30, 5)
Boss = Character("Ganon", 150, 150, 20)


def reinitvariable():
    Hero.hp = Hero.hpmax
    Enemy.hp = Enemy.hpmax
    Boss.hp = Boss.hpmax


def fight(character, floor):
    turn = 1
    while (character.hp > 0) | (Hero.hp > 0):
        print("floor", floor, "turn", turn)
        action = input("""
        what do you want to do ?
        1 - Attack?
        2 - Heal?
        """)
        if action == "":
            action = 1
        action = int(action)
        if action == 1:
            Hero.attack(character)
            if character.hp <= 0:
                print("your enemy was died")
                print(f"you can go up to floor {floor + 1}")
                break
        elif action == 2:
            Hero.heal()
        character.attack(Hero)
        if Hero.hp <= 0:
            floor = 12
            display_ascci_art('game_over.txt')
            quit()
        print(f" {Hero.name} have {Hero.hp} hp")
        print(f" {character.name} have {character.hp} hp")
        turn += 1


def main():
    floor = 1
    while True:
        display_ascci_art("title")
        choice = int(input("""
        1 - Play
        2 - Quit game 
        """))
        choice = int(choice)
        if choice == 1:
            while floor <= boss_floor:
                if floor == boss_floor:
                    fight(Boss, floor)
                    floor += 1
                else:
                    fight(Enemy, floor)
                    floor += 1
                reinitvariable()
            if Hero.hp <= 0:
                display_ascci_art('game_over.txt')
                break
            else:
                display_ascci_art('link.txt')
                return 1
        if choice == 2:
            quit()


if __name__ == '__main__':
    main()
