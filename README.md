# Hyrule Castle Python

## Step 1: Base Game

Before moving on to the mods, it is mandatory to have the basics of the game.

The game is a turn-by-turn RPG. You incarnate a character that challenges the Hyrule Castle, a tower composed of 10
floors. Each floor you encounter an enemy and on the last floor, you challenge the Boss.

For now, your character will be Link. He has 60 maximum health points (HP) and 15 strength (STR).

Each fight you will face a Bokoblin which has 30 HP and 5 STR.

For the 10th floor boss, you will have to defeat “Ganon” who will have 150 HP and 20 STR.

During each fight, you have the choice between “Attack” and “Heal”:

    “Attack” deals damage to the opponent equal to the STR stat of the character
    “Heal” will heal the character by half of his maximum HP

After your character's turn, the opponent attacks and deals damage equal to his STR.

When the opponent's HPs fall to 0, he is defeated and the character climbs one more floor.

When the character's HPs fall to 0, he dies and the game stops.

If the boss is defeated, the game stops with a message of congratulations.

## Step 2: Dynamic Characters

Using the parsing knowledge you acquired, you will fetch the data of the characters from the given JSON files.

These files are available on your intra:

    The player character will be fetched from the players.json file
    The enemies will be fetched from the enemies.json file
    The boss character will be fetched from the bosses.json file

The player character, the enemies, and the boss will be randomly selected, using the rarity as a probability vector (see
Game mechanics > Rarity).

## Step 3: Mods

Now that you have done the base game and added a bit of dynamism to the characters, let's move on to the mods. Your game
is boring and unattractive, but with a few tweaks, you can make it much more interesting.

You can start adding mods to your game. Be careful to respect the prerequisites for each mod, you can't be corrected
otherwise.

Every mod explanation is given in the Better_Combat_Options.html, Basic_Characteristics.html and
Basic_Game_Customization.html files.

You can do as many mods or mod types you wish as long as you respect the requirements.

Good luck!

## Game mechanics

Damage Modifiers

The Damage Modifiers will be used in the following order.

| Modifier              | Formula           | Description                                                            | Example                   |
|-----------------------|-------------------|------------------------------------------------------------------------|---------------------------|
| Base damage           | STR               | The base damage is equal to the STR stat                               | 12                        |
| Critical Strike       | BD × 2            | Double the Base damage                                                 | 12 × 2 = 24               |
| Res/Def               | T - T × (def/100) | Use of the res or def stats on the total                               | 24 - 24 × (8/100) = 22.08 |
| Strength & Weaknesses | T × 2             | Use of the strength/weaknesses multipliers, from /4 to x4 on the total | 22.08 × 2 = 44.16         |
| Fear                  | T / 2             | Halve the total damage                                                 | 44.16 / 2 = 22.08         |
| Weakening             | T × 1.5           | Add +50% damage on the total damage                                    | 22.08 × 1.5 = 33.12       |

Finally, the total value has to be rounded down to the whole number. Here, 16.56 will be reduced to 16.

## Rarity

Some random events are influenced by rarity. It is the case for player and enemies choice, item drops, etc. Rarity is
the first value used to calculate the odd of “choosing” an entity or an item.

For instance, when the player drops an item, first the rarity of the dropped item will be calculated, and then a random
item from the chosen rarity will be given. The same goes for every enemy encountered or playable character.

Each rarity has different chances to be chosen:

| Rarity Tier | Percentage | Example                             |
|-------------|------------|-------------------------------------|
| 0           | 0%         | Special undropables skill and items |
| 1           | 50%        | Low-level Potion, Skulltula, Link   |
| 2           | 30%        | Potion, Lizalfos, Young Link        |
| 3           | 15%        | Good Potion, Dead Hand, Sheik       |
| 4           | 4%         | High-level Potion, Stalfos, Impa    |
| 5           | 1%         | Holy Potion, Guardian, Hylia        |
